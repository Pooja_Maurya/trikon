package com.example.logindemo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
EditText email;
EditText password;
ImageView login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email=findViewById(R.id.etEmail);
        password=findViewById(R.id.etPassword);
        login=findViewById(R.id.imLogin);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String e=email.getText().toString();
                String p=password.getText().toString();
                if (e.isEmpty() || p.isEmpty())
                    Toast.makeText(MainActivity.this,"All the field required",Toast.LENGTH_LONG).show();
                else {
                    Toast.makeText(MainActivity.this, e + "  " + p, Toast.LENGTH_LONG).show();
                    showCustomDialog();
                }
            }
        });
    }

    private void showCustomDialog() {
        final ProgressDialog dialog=new ProgressDialog(this);
        dialog.setMessage("wait a sec..");
        dialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.dismiss();
                Intent intent=new Intent(MainActivity.this,HomPage.class);
                startActivity(intent);
            }
        },8000);
    }
}
